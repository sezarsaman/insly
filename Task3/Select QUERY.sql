/* GET data for employee_id = 1 */
SET @var_id = 1; 
SELECT 
	e.name,e.birthday,e.ssn,e.email,e.address,
    (SELECT u.user_name FROM users u WHERE u.user_id = e.last_modified_by) modifier,e.last_modified_date,
    (SELECT u.user_name FROM users u WHERE u.user_id = e.created_by) creator,e.created_time,
    CONCAT('[',GROUP_CONCAT(ei.introduction),']') introduction,
    CONCAT('[',GROUP_CONCAT(ei.career),']') career,
    CONCAT('[',GROUP_CONCAT(ei.education),']') education
FROM 
	employees e 
INNER JOIN 
	employees_information ei 
ON 
	(e.employee_id=ei.employee_id)
WHERE 
	e.employee_id = @var_id
GROUP BY 
	e.employee_id
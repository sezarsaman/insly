<?php

/**
 * @author Saman Mohammadi
 * @copyright 2019
 * Task #1 : print out your name with one of php loops
 */
$text = "saman";
$text_array = str_split($text);
$alphas = range('a', 'z');
for($i=0;$i<count($text_array);$i++){
    foreach($alphas as $alpha){
        if($alpha==$text_array[$i]){
            echo $alpha;
            break;
        }
    }
}
?>